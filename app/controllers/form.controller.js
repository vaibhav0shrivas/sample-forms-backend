const db = require("../models");
const Users = db.users;
const Forms = db.forms;
const Operator = db.Sequelize.Op;

const queryForms = (req, res) => {
  const { userId } = req.query;
  if (userId === undefined) {
    getAllForms(req, res);

  } else {
    getAllFormsForUser(req, res, userId);
  }
};

const getAllFormsForUser = (req, res) => {
  const userId = req.query.userId;

  Users.findByPk(userId)
    .then(user => {
      if (user === null) {
        throw new Error("Users not present in database.");
      } else {
        return Forms.findAll({
          attributes: [
            ["id", "form_id"],
            "form_name",
            "form_description",
            "form_fields"
          ],
          include: [
            {
              model: Users,
              as: "users_assigned",
              through: {
                attributes: []
              },
              attributes: [
              ],
              where: {
                id: userId
              }
            }
          ]
        });
      }
    }).then(data => {
      res.send(data);
    })
    .catch(err => {

      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving forms."
      });
    });


};


const editFormAssignments = async (req, res) => {
  const form_id_in_req = req.params.formId;
  const users_assigned_email = req.body.users_assigned;

  try {
    const form = await Forms.findByPk(form_id_in_req);
    if (form === null) {
      res.status(500).send({
        message:
          `Form with form_id ${form_id_in_req} not present in database.`
      });
      return;
    }
  }
  catch (err) {
    res.status(500).send({
      message:
        err.message
    });
    return;
  }

  if (users_assigned_email.length === 0) {
    res.status(500).send({
      message:
        `Empty array of users sent!`
    });

  } else {

    Users.findAll({
      where: {
        user_email: {
          [Operator.in]: users_assigned_email
        }
      }
    })
      .then(async users => {
        if (users.length === 0) {
          res.status(500).send({
            message: `Users not present in database`
          });

        } else {

          try {
            for (const user of users) {
              await db.form_assignments.create({
                formId: form_id_in_req,
                userId: user.id
              });
            }
          } catch (err) {
            throw err;
          }

        }

      })
      .then((data) => {
        if (data !== [] || data !== undefined) {
          res.status(200).send({
            message: `Form with form_id ${form_id_in_req} assigned to users with email ${users_assigned_email}`
          });
        } else {
          throw new Error("Users not present in database.")
        }

      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || `Some error occurred while assigning form with id ${formId} to new users.`
        });
      });

  }

}

const getAllForms = (req, res) => {
  Forms.findAll({
    attributes: [
      ["id", "form_id"],
      "form_name",
      "form_description",
      "form_fields"
    ],
    include: [
      {
        model: Users,
        as: "users_assigned",
        through: {
          attributes: []
        },
        attributes: [
          'id',
          'user_name',
          'user_email'
        ]
      }
    ]
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {

      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving forms."
      });
    });

};

const findFormById = (req, res) => {
  const formId = parseInt(req.params.formId);
  Forms.findByPk(formId,
    {
      attributes: [
        ["id", "form_id"],
        "form_name",
        "form_description",
        "form_fields"
      ],
      include: [
        {
          model: Users,
          as: "users_assigned",
          through: {
            attributes: []
          },
          attributes: [
            'id',
            'user_name',
            'user_email'
          ]
        }
      ]
    })
    .then(data => {

      if (data === null) {
        res.status(500).send({
          message:
            `Form with id ${formId} does not exist.`
        });
      } else {

        res.send(data);
      }
    })
    .catch(err => {

      res.status(500).send({
        message:
          err.message || `Some error occurred while retrieving form with id ${formId}.`
      });
    });

};

const createForm = (req, res) => {

  const users_assigned_email = req.body.users_assigned;
  let responseJSON = null;

  Forms.create(req.body,
    {
      fields: [
        "form_name",
        "form_description",
        "form_fields",
      ]
    })
    .then(data => {
      responseJSON = data;

      return Users.findAll({
        where: {
          user_email: {
            [Operator.in]: users_assigned_email
          }
        }
      });

    })
    .then(async users => {
      if (users.length === 0) {
        res.status(200).send(responseJSON);

      } else {

        try {
          for (const user of users) {
            await db.form_assignments.create({
              formId: responseJSON.id,
              userId: user.id
            });
          }
        } catch (err) {
          throw err;
        }

      }


    }).then((data) => {
      if (data !== [] || data !== undefined) {
        res.status(200).send(responseJSON);
      } else {
        throw new Error("Users not present in database.")
      }

    })
    .catch(err => {

      res.status(500).send({
        message:
          err.message || `Some error occurred while retrieving form with id ${formId}.`
      });
    });

};

module.exports = {
  editFormAssignments,
  getAllForms,
  findFormById,
  createForm,
  queryForms
};