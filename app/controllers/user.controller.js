const db = require("../models");
const Users = db.users;

const findAllUsers = (req, res) => {
    Users.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {

            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

module.exports ={
    findAllUsers
};