const db = require("../models");
const Users = db.users;
const Forms = db.forms;
const Form_responses = db.form_responses;

const queryResponse = (req, res) => {
  const { formId, userId } = req.query;
  if (formId === undefined && userId === undefined) {
    res.status(500).send({
      message:
        `Invalid query formId and userId is empty.`
    });
  } else if (userId === undefined && formId !== undefined) {
    getAllResponsesForFormId(req, res, formId);

  } else if (formId === undefined && userId !== undefined) {
    getFormResponsesForUser(req, res);

  } else {
    getFormResponseForFormAndUser(req, res, formId, userId);
  }
}

const getResponseForUserAndFormX = async (req, res) => {
  const { formId, userId } = req.query;
  if (formId === undefined || userId === undefined) {
    res.status(500).send({
      message:
        `Invalid query formId and userId is empty.`
    });
  } else {
    try {
      const form = await Forms.findByPk(formId);
      if (form === null) {
        res.status(500).send({
          message:
            `Form with form_id ${formId} not present in database.`
        });
        return;
      }

      const user = await Users.findByPk(userId);
      if (user === null) {
        res.status(500).send({
          message:
            `User with user_id ${userId} not present in database.`
        });
        return;
      }

      Form_responses.findOne({
        attributes: [
          ["form_fields_with_response","form_fields"],
          "form_id",
          "user_id",
        ],
        where: {
          form_id: formId,
          user_id: userId
        },
        include: [{
          model: Forms,
          as: "responses",
          attributes: [
            "form_name",
            "form_description",
          ],
        }],
        raw: true

      })
        .then(data => {
          if (data === null) {
            res.status(500).send({
              message:
                `No Form response from user ${userId} for form ${formId}`
            });
            return;
          }

          data["form_name"] = data["responses.form_name"];
          data["form_description"] = data["responses.form_description"];
          delete data["responses.form_name"];
          delete data["responses.form_description"];
          res.send(data);
        })
        .catch(err => {

          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving users."
          });
        });



    }
    catch (err) {
      res.status(500).send({
        message:
          err.message
      });
      return;
    }



  }

}


const getFormResponsesForUser = (req, res) => {

  Forms.findAll({
    attributes: [
      ["id", "form_id"],
      "form_name",
      "form_description"
    ],
    include: [{
      model: Form_responses,
      as: 'responses',
      attributes: [
        ['user_id', 'user_id'],
        'form_fields_with_response',
      ],
      where: {
        user_id: req.query.userId
      }
    }]

  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {

      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });


}

const addFormResponse = (req, res) => {
  const { form_id, user_id } = req.body;

  Forms.findByPk(form_id,
    {
      include: [
        {
          model: Users,
          as: "users_assigned",
          through: {
            attributes: []
          },
          attributes: [
            'id',
            'user_name',
            'user_email'
          ],
          where: {
            id: user_id
          }
        }
      ]
    })
    .then(form_being_filled => {

      if (form_being_filled === null || form_being_filled.length === 0) {
        throw new Error("Invalid form_id and user_id combination.")
      } else {

        return Form_responses.create({
          ...req.body,
          userId: form_being_filled.users_assigned[0].id,
          formId: form_being_filled.id

        },
          {
            fields: [
              "form_fields_with_response",
              "userId",
              "formId"
            ]
          });
      }


    })
    .then(data => {

      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });

};

const getAllResponsesForFormId = (req, res, formId) => {
  const form_id_in_req = formId;

  Forms.findAll({
    attributes: [
      ["id", "form_id"],
      "form_name",
      "form_description"
    ],
    where: {
      id: form_id_in_req
    },
    include: [{
      model: Form_responses,
      as: 'responses',
      attributes: [
        ['user_id', 'user_id'],
        'form_fields_with_response',
      ],
    }]

  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {

      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });

};

const getFormResponseForFormAndUser = (req, res, formId, userId) => {
  const form_id_in_req = formId;
  const user_id_in_req = userId;

  Forms.findAll({
    attributes: [
      ["id", "form_id"],
      "form_name",
      "form_description"
    ],
    where: {
      id: form_id_in_req
    },
    include: [{
      model: Form_responses,
      as: 'responses',
      attributes: [
        ['user_id', 'user_id'],
        'form_fields_with_response',
      ],
      where: {
        user_id: user_id_in_req
      }
    }]

  })
    .then(data => {

      if (data.length === 0) {
        throw new Error("Invalid form_id and user_id combination for form response.")
      } else {
        res.send(data);
      }

    })
    .catch(err => {

      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });

};

const editFormResponse = (req, res) => {
  const form_id_in_req = req.params.formId;
  const user_id_in_req = req.params.userId;
  const updated_form_fields_with_response = req.body.form_fields_with_response;

  Forms.findByPk(form_id_in_req,
    {
      include: [
        {
          model: Users,
          as: "users_assigned",
          through: {
            attributes: []
          },
          attributes: [
            'id',
            'user_name',
            'user_email'
          ],
          where: {
            id: user_id_in_req
          }
        }
      ]
    })
    .then(form_being_filled => {

      if (form_being_filled === null || form_being_filled.length === 0) {
        throw new Error("Invalid form_id and user_id combination.")
      } else {

        return Form_responses.update({
          form_fields_with_response: updated_form_fields_with_response,
        },
          {
            where: {
              form_id: form_id_in_req,
              user_id: user_id_in_req

            }
          }
        );
      }
    })
    .then(() => {

      res.send({
        message: `Response updated for user_id ${user_id_in_req} in form_id ${form_id_in_req}`
      });

    })
    .catch(err => {

      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });

    });

};


module.exports = {
  queryResponse,
  addFormResponse,
  getAllResponsesForFormId,
  getFormResponseForFormAndUser,
  editFormResponse,
  getResponseForUserAndFormX
};