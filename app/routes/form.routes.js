module.exports = app => {
  const form = require("../controllers/form.controller.js");
  const user = require("../controllers/user.controller.js");
  const response = require("../controllers/response.controller.js");

  const router = require("express").Router();


  router.get("/users", user.findAllUsers);
  router.get("/form", form.queryForms);
  router.get("/form/:formId", form.findFormById);
  router.post("/form/", form.createForm);
  router.post("/response/", response.addFormResponse);
  router.get("/response/", response.queryResponse);
  router.post("/response/:formId/:userId", response.editFormResponse);
  router.post("/form/:formId", form.editFormAssignments);
  router.get("/responseForUserAndForm/",response.getResponseForUserAndFormX)



  app.use("/api/v1/", router);
};
