module.exports = (sequelize, Sequelize) => {

    const Form_responses = sequelize.define("form_responses", {
        form_fields_with_response:{
            type: Sequelize.JSON,
            allowNull: false,
        },
        
    },
        {

            freezeTableName: true,
            underscored: true,
            timestamps: false,
        }
    );

    return Form_responses;
};




