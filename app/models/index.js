const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  port: dbConfig.PORT,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("./users_model.js")(sequelize, Sequelize);
db.forms = require("./forms_model.js")(sequelize, Sequelize);
db.form_assignments = require("./form_assignments_model.js")(sequelize, Sequelize);
db.form_responses = require("./form_responses_model.js")(sequelize, Sequelize);

db.forms.hasMany(db.form_responses,
  {
    as: "responses"
  }
);
db.form_responses.belongsTo(db.forms,
  {
    foreignKey: "formId",
    as: "responses"
  }
);

db.users.hasMany(db.form_responses,
  {
    as: "submissions"
  }
);
db.form_responses.belongsTo(db.users,
  {
    foreignKey: "userId",
    as: "user_id"
  }
);

db.users.belongsToMany(db.forms,
  {
    through: db.form_assignments,
    unique: false
  }
);

db.forms.belongsToMany(db.users,
  {
    through: db.form_assignments,
    as:"users_assigned",
    unique: false
  }
);



module.exports = db;
