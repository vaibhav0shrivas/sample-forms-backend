module.exports = (sequelize, Sequelize) => {

    const Forms = sequelize.define("forms", {
        form_name: {
            type: Sequelize.STRING,        
        },
        form_description: {
            type: Sequelize.STRING,
        },
        form_fields:{
            type: Sequelize.JSON,
            allowNull: false,
        }
    },
        {

            freezeTableName: true,
            underscored: true,
            timestamps: false,
        }
    );

    return Forms;
};
