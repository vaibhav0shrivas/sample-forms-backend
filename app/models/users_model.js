module.exports = (sequelize, Sequelize) => {

    const Users = sequelize.define("users", {
        user_name: {
            type: Sequelize.STRING
        },
        user_email: {
            type: Sequelize.STRING,
            unique: true,

        }
    },
        {

            freezeTableName: true,
            timestamps: false,
            underscored: true,
        }
    );

    return Users;
};
