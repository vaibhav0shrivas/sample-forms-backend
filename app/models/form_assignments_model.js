module.exports = (sequelize, Sequelize) => {

    const Form_assignments = sequelize.define("form_assignments", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },

    },
        {

            freezeTableName: true,
            underscored: true,
            timestamps: false,
        }
    );

    return Form_assignments;
};