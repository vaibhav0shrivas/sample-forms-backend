# Google Forms like backend with Node.js PostgreSQL Express.js

# Endpoints

### POST /api/v1/form/
    Endpoint to create a form.

### POST /api/v1/form/:formId
    Endpoint to add users to a already created form.

### GET /api/v1/form/:formId
    Endpoint to get a particular form.

### GET /api/v1/form/
    Endpoint to get a all forms.

### GET /api/v1/users/
    Endpoint to get all users.

### POST /api/v1/response/
    Endpoint to add response, body needs to contain form_id and user_id.

### POST /api/v1/response/:formId/:userId
    Endpoint to edit a response.

### GET /api/v1/response?formId={formId}&userId={userId}
    Endpoint to get a particular user's response for particular form.